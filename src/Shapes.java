import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Shapes {

    protected static List<String> getAll() {
        List<String> textFiles = new ArrayList<String>();
        File dir = new File("weights/");
        if (!dir.exists()) {
            dir.mkdir();
        }
        for (File file : dir.listFiles()) {
            String fileName = file.getName();
            if (fileName.toLowerCase().endsWith((".wei")))
                textFiles.add(fileName.substring(0, fileName.length() - 4));
        }
        return textFiles;
    }

    protected static void removeShape(String s) {
        File f = new File("weights/" + s + ".wei");
        if (!f.exists())
            throw new IllegalArgumentException(
                    "Delete: no such file or directory: " + f.getAbsolutePath());
        if (!f.canWrite())
            throw new IllegalArgumentException("Delete: write protected: "
                    + f.getAbsolutePath());
        f.delete();
    }

    protected static float[][] getShapeWeights(String shape) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream("weights/" + shape + ".wei"));
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String str = br.readLine();
            String[] strLine = str.split(";");
            float[][] weights = new float[strLine.length][];
            for (int i = 0; i < strLine.length; i++) {
                String[] weight = strLine[i].split(",");
                weights[i] = new float[weight.length];
                for (int j = 0; j < weight.length; j++) {
                    float w = Float.valueOf(weight[j]);
                    weights[i][j] = ((w < 0) ? 0 : ((w > 1) ? 1 : w));
                }
            }
            in.close();
            return weights;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static void addShape(String s) throws IOException {
        float[][] defWeights = new float[200][];
        for (int i = 0; i < defWeights.length; i++) {
            defWeights[i] = new float[200];
            for (int j = 0; j < 200; j++)
                defWeights[i][j] = 0;
        }
        writeWeightsToFile(s, defWeights);
    }

    public static void writeWeightsToFile(String s, float[][] weights) throws IOException {
        FileOutputStream fos = getFOS(s);
        for (float[] weight : weights) {
            for (float aWeight : weight)
                fos.write((String.valueOf(aWeight) + ",").getBytes());
            fos.write(";".getBytes());
        }
        fos.flush();
        fos.close();
    }

    public static FileOutputStream getFOS(String s) throws FileNotFoundException {
        File yourFile = new File("weights/" + s + ".wei");
        if (!yourFile.exists()) {
            try {
                yourFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (new FileOutputStream(yourFile, false));
    }
}
