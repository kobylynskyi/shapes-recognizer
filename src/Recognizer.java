import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.List;

public class Recognizer extends JFrame {
    private JButton clearButton;
    private JPanel contentPane;
    private JButton recognizeButton;
    private JButton correctButton;
    private JButton wrongButton;
    private JLabel resultLabel;
    private JPanel choosePanel;
    private JButton shapesManagerButton;
    private JPanel drawingPanelContainer;
    private JSpinner amountOfRunsSpinner;
    private JLabel amountOfRunsLabel;
    private JLabel readyLabel;
    private JLabel drawLabel;
    private DrawingPanel panel;

    public Recognizer() {
        initComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        panel.clear();
        setTitle("Shapes recognition system");

        recognizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Shapes.getAll().size() >= 2) {
                    readyLabel.setVisible(false);
                    drawLabel.setText(" ");
                    recognizeButton.setVisible(false);
                    BufferedImage image = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g2d = image.createGraphics();
                    panel.paint(g2d);
                    g2d.dispose();
                    resultLabel.setText("This is " + NeuralNetwork.getPrediction(image).getName().toLowerCase() + ".");
                    choosePanel.setVisible(true);
                    correctButton.setVisible(true);
                    wrongButton.setVisible(true);
                    resultLabel.setVisible(true);
                    pack();
                } else {
                    JOptionPane.showMessageDialog(Recognizer.this, "Add to the network memory at least two shapes classes.");
                    panel.clear();
                }
            }
        });

        correctButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NeuralNetwork.refreshWeightsForCorrect(
                        NeuralNetwork.getPredicted(),
                        (Integer) amountOfRunsSpinner.getValue());
                choosePanel.setVisible(false);
                correctButton.setVisible(false);
                wrongButton.setVisible(false);
                resultLabel.setVisible(false);
                readyLabel.setVisible(true);
                drawLabel.setText("1. Draw figure:");
                recognizeButton.setVisible(true);
                panel.clear();
                pack();
            }
        });

        wrongButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> allShapes = Shapes.getAll();
                Neuron correctNeuron = NeuralNetwork.getNeuronByName(
                        (String) JOptionPane.showInputDialog(Recognizer.this,
                                "Choose correct figure, that corresponds drawed image", "Figures",
                                JOptionPane.INFORMATION_MESSAGE, null,
                                allShapes.toArray(new String[allShapes.size()]), allShapes.get(0))
                );
                NeuralNetwork.refreshWeightsForCorrect(correctNeuron, (Integer) amountOfRunsSpinner.getValue());

                choosePanel.setVisible(false);
                correctButton.setVisible(false);
                wrongButton.setVisible(false);
                resultLabel.setVisible(false);
                readyLabel.setVisible(true);
                drawLabel.setText("1. Нарисуйте контур фигуры:");
                recognizeButton.setVisible(true);
                panel.clear();
                pack();
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choosePanel.setVisible(false);
                correctButton.setVisible(false);
                wrongButton.setVisible(false);
                resultLabel.setVisible(false);
                readyLabel.setVisible(true);
                drawLabel.setText("1. Draw figure:");
                recognizeButton.setVisible(true);
                panel.clear();
                pack();
            }
        });

        shapesManagerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ShapesManager().setVisible(true);
            }
        });
    }

    private void initComponents() {
        setContentPane(contentPane);
        setResizable(false);
        setLocation(150, 150);
        drawingPanelContainer.setPreferredSize(new Dimension(200, 200));
        panel = new DrawingPanel();
        panel.setPreferredSize(new Dimension(200, 200));
        drawingPanelContainer.setLayout(new BorderLayout());
        drawingPanelContainer.add(panel, BorderLayout.CENTER);
        choosePanel.setVisible(false);
        correctButton.setVisible(false);
        wrongButton.setVisible(false);
        resultLabel.setVisible(false);
        amountOfRunsSpinner.setValue(1);
    }

}
