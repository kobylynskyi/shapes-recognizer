public class Neuron {

    private int[][] input;
    private float[][] weights;
    private String name;
    private float sum;

    public Neuron(int[][] input, String name) {
        this.name = name;
        this.input = input;
        this.weights = Shapes.getShapeWeights(name);
        sumW();
    }

    private void sumW() {
        for (int i = 0; i < input.length; i++)
            for (int j = 0; j < input[i].length; j++)
                sum += input[i][j] * weights[i][j];
    }

    protected float getSum() {
        return sum;
    }

    protected String getName() {
        return name;
    }

    public float[][] getWeights() {
        return weights;
    }

}
